/**
 * Jenkinsfile pipeline for a DevOps CI CD deployment of database changes.
 *
 * The pipeline can be easily modified to include additional steps, such as building and deploying your application,
 * or running additional tests. It can also be integrated with other tools in a CI/CD workflow, such as Jira, Confluence
 * or Docker, to provide a seamless and automated testing experience.
 *
 * This script is parameterized and uses the following parameters to execute:
 *
 * @param TAG
 *                      -  The git tag to checkout and execute this job
 * @param ACTION
 *                      -  "Update" or "Rollback" action to execut
 * @param ROLLBACK_TAG
 *                      -  Rollback-to parameter (e.g. 1.0.0)
 * @param SCHEMA
 *                      -  Schema folder of the database change-logs
 * @param STAGE
 *                      - The environment in which to execute the tests. This can be a ENTW, USER or QSS environment that you want to run tests.
 * @param RECIPIENTS    - A list of email recipients to whom a mail with the job information should be sent.
 *                        This parameter is useful if you want to notify specific people when the job completes or fails.
 *
 * @author Serano Colameo
 */

def user

node {
    wrap([$class: 'BuildUser']) {
        user = env.BUILD_USER_ID
    }

    emailext mimeType: 'text/html',
            subject: "The deployment job [${currentBuild.fullDisplayName}] is waiting for approval",
            to: "scolameo@ethz.ch, silviomo@ethz.ch",
            body: '''<a href="${BUILD_URL}input">Click here to approve or abort the provisioning request...</a>'''
}

pipeline {

    agent {
        label 'Jenkins'
    }

    environment {
        SANDBOX = false
        JAVA_HOME = "/etc/alternatives/java_sdk_11_openjdk"
        MAVEN_ID = "cb5ec4c1-27a9-4b79-9cc6-7028fcf2982b"
        GIT_URL = "https://gitlab.ethz.ch/agile-db-mgmt/dbmgmt.git"
        GIT_CREDENTIALS = "6d82e497-6c05-4e47-9631-39360fd72b74"
    }

    options {
        disableResume()
        disableConcurrentBuilds(abortPrevious: true)
        buildDiscarder(logRotator(daysToKeepStr: '7', numToKeepStr: '10'))
        timeout(time: 1, unit: 'HOURS')
    }

    parameters {
        gitParameter(name: 'TAG', type: 'PT_TAG', defaultValue: 'tag', branchFilter: 'main', sortMode: 'DESCENDING_SMART', description: 'Release tag to use for the database change')
        gitParameter(name: 'ROLLBACK_TAG', type: 'PT_TAG', defaultValue: 'tag', branchFilter: 'main', sortMode: 'DESCENDING_SMART', description: 'Release tag to set/use to revert the database change (should be the tag before the selected release tag)')
        choice(name: 'ACTION', choices: ['update', 'rollback'], description: 'Which action should be applied to the database?')
        string(name: 'SCHEMA', description: 'Schema folder of the database change-logs (e.g. TEST_SER_DEPL)')
        choice(name: 'STAGE', choices: ['ENTW', 'USER', 'QSS', 'PROD'], description: 'In which environment should this job deploy the database changes?')
        string(name: 'RECIPIENTS', defaultValue: 'scolameo@ethz.ch, silviomo@ethz.ch', description: 'Comma separated list of email recipients to be notified via email')
    }

    stages {

        stage('Clean workspace first') {
            steps {
                sh "rm -rf ${WORKSPACE}/dbmgmt"
                sh "mkdir -p ${WORKSPACE}/dbmgmt"
            }
        }

        stage('Prepare execution') {
            when {
                expression {
                    return params.SCHEMA.isEmpty() || params.RECIPIENTS.isEmpty()
                }
            }
            steps {
                script {
                    def undeferr = ' is undefined';
                    def reason = 'parameter ' + (params.SCHEMA.trim().isEmpty() ? "SCHEMA" + undeferr :
                            params.RECIPIENTS.trim().isEmpty() ? "RECIPIENTS" + undeferr : 'error');
                    currentBuild.result = 'ABORTED'
                    error("Aborting - ${reason}!")
                }
            }
        }

        stage('Git checkout tag') {
            steps {
                git branch: "main", credentialsId: "${env.GIT_CREDENTIALS}", url: "${env.GIT_URL}"
                sh "git checkout tags/${params.TAG}"
            }
        }

        stage('Tag database') {
            when {
                expression {
                    return params.ACTION == 'update' && params.TAG != params.ROLLBACK_TAG
                }
            }
            steps {
                withMaven(globalMavenSettingsConfig: "${env.MAVEN_ID}", maven: 'Maven') {
                    sh "mvn clean verify -PtagTargetChangeLog -Ddb.schema=${params.SCHEMA} -Ddb.version=${params.ROLLBACK_TAG} -Ddb.target=${params.STAGE}"
                }
            }
        }

        stage('Get approval for deployment') {
            input {
                message "Please approve or abort the ${params.ACTION} of ${params.SCHEMA} to v${params.ACTION == 'rollback' ? params.ROLLBACK_TAG : params.TAG} in ${params.STAGE}"
                ok 'Approve'
                submitterParameter 'approverId'
            }
            when {
                expression { user in ['silviomo', 'scolameo'] }
            }
            steps {
                echo "Database change [${params.ACTION} of ${params.SCHEMA} to v${params.ACTION == 'rollback' ? params.ROLLBACK_TAG : params.TAG} in ${params.STAGE}] was approved by ${approverId}"

                withMaven(globalMavenSettingsConfig: "${env.MAVEN_ID}", maven: 'Maven') {
                    script {
                        env.ROLLBACK_TO = params.ACTION == "rollback" ? "-Dliquibase.rollbackTag=${params.ROLLBACK_TAG}" : ""
                    }
                    sh "mvn clean verify -P${params.ACTION}TargetChangeLog -Ddb.schema=${params.SCHEMA} -Ddb.version=${params.TAG} -Ddb.target=${params.STAGE} ${env.ROLLBACK_TO}"
                }
            }
        }
    }

    post {
        always {
            emailext body:
                """
                Jenkins job ${env.JOB_NAME} has status ${currentBuild.currentResult} on build ${currentBuild.getNumber()}. \n\nGo to ${BUILD_URL} for more information.
                """,
                subject: "Jenkins job ${env.JOB_NAME} has status ${currentBuild.currentResult}",
                to: "${params.RECIPIENTS}"
        }
        changed {
            emailext body: "Go to ${BUILD_URL} for more information.",
                    subject: "Jenkins job ${env.JOB_NAME} changed to ${currentBuild.currentResult}",
                    to: "${params.RECIPIENTS}"
        }
    }
}