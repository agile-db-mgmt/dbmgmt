# Database-Management CI/CD with Liquibase and Jenkins

## Getting started

Clone this repo:
```
git clone https://gitlab.ethz.ch/agile-db-mgmt/dbmgmt.git
cd dbmgmt
```

### Installation
- In order to test on your local machine, install [Docker desktop](https://www.docker.com/products/docker-desktop/)
- Pull oracle-xe image and create a Docker container as follows:

```
docker pull gvenzl/oracle-xe:latest
```

- Before starting a container, you need to clean up an already existing instance:
```
docker rm -f $(docker ps -aq)
```

- Start a new container as follows:
  - Open a terminal/shell
    - cd <this-repo-root-folder>
    - on Unix OS:
        ```
        docker run --name TEST_SER_DEPL \
        -p 1521:1521 \
        -e ORACLE_PASSWORD=CICDLiquiBINT2 \
        -v ${pwd}/src/main/resources/liquibase/TEST_SER_DEPL/db.init.oracle.sql:/container-entrypoint-initdb.d \
        gvenzl/oracle-xe:latest
        ```
    - On Windows:
        ```
        docker run --name TEST_SER_DEPL -p 1521:1521 -e ORACLE_PASSWORD=CICDLiquiBINT2 -v %cd%\src\main\resources\liquibase\TEST_SER_DEPL\init:/container-entrypoint-initdb.d gvenzl/oracle-xe:latest
        ```
- Login using a SQL tool (e.g. SQL Developer or another):
  - ![image search api](doc/login-sqldev.png)

- Check the oracle version:
  ```
  SELECT * FROM v$version;
  ```

- Login to TEST_SER_DEPL_1_MGR-Source/CICDLiquiBDEV1 with the Service-Name XEPDB1:
  - JDBC-URL: 
  ```
  jdbc:oracle:thin:@(DESCRIPTION=(FAILOVER=ON)(LOAD_BALANCE=OFF)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=localhost)(PORT=1521))(ADDRESS=(PROTOCOL=TCP)(HOST=localhost)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=XEPDB1)))
  ```
  - Using SQL-Developer:
    ![image search api](doc/login-TEST_SER_DEPL_1_MGR-Source-sqldev.png)

- Login to TEST_SER_DEPL_1_MGR_Target/CICDLiquiBINT1 with the Service-Name XEPDB1:
    - JDBC-URL:
  ```
  jdbc:oracle:thin:@(DESCRIPTION=(FAILOVER=ON)(LOAD_BALANCE=OFF)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=localhost)(PORT=1521))(ADDRESS=(PROTOCOL=TCP)(HOST=localhost)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=XEPDB1)))
  ```
    - Using SQL-Developer:
      ![image search api](doc/login-TEST_SER_DEPL_1_MGR-Target-sqldev.png)

- For more details about connectivity see:
  - [```base.properties```](src/main/resources/liquibase/TEST_SER_DEPL/config/base.properties)
  - [```source.properties```](src/main/resources/liquibase/TEST_SER_DEPL/config/source.properties)
  - [```target.local.properties```](src/main/resources/liquibase/TEST_SER_DEPL/config/target.local.properties)
  - ```target.<ENV>.properties```
  - ...

- Check that both (source/target) databases are empty:
  ```
  select * from tabs;
  ```

## Export from source schema and migrate to target database

1. Export the existing schema:
   ```
   ./mvnw clean verify -PgenerateSourceChangeLog -Ddb.schema=TEST_SER_DEPL -Ddb.version=1.0.0 -Ddb.target=local \
    -Dliquibase.diffTypes=catalogs,tables,functions,views,columns,indexes,foreignkeys,primarykeys,uniqueconstraints,storedprocedures,triggers,sequences,databasepackage,databasepackagebody
   ```

2. Import into the local source database or simply create a database copy with or without data:

   | Skip this step if you decided to create a database copy! |
   |----------------------------------------------------------|
   ```
   ./mvnw clean verify -PupdateSourceChangeLog -Ddb.schema=TEST_SER_DEPL -Ddb.version=1.0.0 -Ddb.target=local
   ```
   
3. Create base schema for the local target database:
   ```
   ./mvnw clean verify -PupdateTargetChangeLog -Ddb.schema=TEST_SER_DEPL -Ddb.version=1.0.0 -Ddb.target=local
   ```

4. Apply changes to the source database manually or with a tool (e.g. SQL Developer) of your preference
   ```
   ...
   ```

5. Export a difference change log in a new release folder as follows:
   ```
   ./mvnw clean verify -PdiffSourceChangeLog -Ddb.schema=TEST_SER_DEPL -Ddb.version=1.0.1 -Ddb.target=local
   ```

   | Please be aware that you need to provide rollback statements in the generated SQL script, otherwise Liquibase cannot undo changes! | 
   |-------------------------------------------------------------------------------------------------------------------------------------|

   e.g. to add a column and undo the change:
   ```
   -- changeset scolameo:1694155462614-1
   ALTER TABLE RM_ADRESSE ADD COLUMN1 NUMBER(38, 0);
   -- rollback ALTER TABLE RM_ADRESSE DROP COLUMN COLUMN1;
   ```

6. Review the generated SQL scripts and if ok
   - [```..TEST_SER_DEPL/1.0.0/db.changelog.oracle.sql```](src/main/resources/liquibase/TEST_SER_DEPL/1.0.0/db.changelog.oracle.sql)
   - [```..TEST_SER_DEPL/1.0.1/db.changelog.oracle.sql```](src/main/resources/liquibase/TEST_SER_DEPL/1.0.1/db.changelog.oracle.sql)
   - [```..TEST_SER_DEPL/db.main.xml```](src/main/resources/liquibase/TEST_SER_DEPL/db.main.xml)

    and add an entry for the change-log to the db.main.xml
    ```xml
   <databaseChangeLog
   xmlns="http://www.liquibase.org/xml/ns/dbchangelog"
   xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
   xsi:schemaLocation="http://www.liquibase.org/xml/ns/dbchangelog http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-3.8.xsd">

    <!-- Including SQL changes from release 1.0.x -->
    <include file="1.0.0/db.changelog.oracle.sql" relativeToChangelogFile="true"/>
    <include file="1.0.1/db.changelog.oracle.sql" relativeToChangelogFile="true"/>
    ...
    ```

7. Before continuing set a tag to be able to rollback:
   ```
   ./mvnw clean verify -PtagTargetChangeLog -Ddb.schema=TEST_SER_DEPL -Ddb.version=1.0.0 -Ddb.target=local
   ```

8. To apply and test all changes locally, use the following Maven command:
   ```
   ./mvnw clean verify -PupdateTargetChangeLog -Ddb.schema=TEST_SER_DEPL -Ddb.version=1.0.1 -Ddb.target=local
   ```

9. To rollback changes use the following Maven command:
   ```
   ./mvnw clean verify -ProllbackTargetChangeLog -Ddb.schema=TEST_SER_DEPL -Ddb.version=1.0.1 -Ddb.target=local -Dliquibase.rollbackTag=1.0.0
   ```

10. That's it, we have now a local database which is exactly how it will be deployed.

    | Do not forget to tag, commit and push your changes! |
    |-----------------------------------------------------|

## Manage changes with Jenkins
- ![image search api](doc/jenkins-pipeline.png)

## Authors and acknowledgment
* Serano Colameo

## License
* Copyright (c) 2023 ETH Zurich
* All rights reserved. 

## Project status
- Work in Progress
