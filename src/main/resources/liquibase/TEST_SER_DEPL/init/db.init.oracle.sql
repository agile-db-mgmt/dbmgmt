ALTER SESSION SET CONTAINER=XEPDB1;
create bigfile tablespace studadm datafile 'STUDADM.dat' size 10M autoextend on;
create user TEST_SER_DEPL_1_MGR_Source identified by CICDLiquiBDEV1 DEFAULT TABLESPACE STUDADM quota unlimited on users;
grant sysdba, connect, resource, unlimited tablespace, create view to TEST_SER_DEPL_1_MGR_Source;
create user TEST_SER_DEPL_1_MGR_Target identified by CICDLiquiBINT1 DEFAULT TABLESPACE STUDADM quota unlimited on users;
grant sysdba, connect, resource, unlimited tablespace, create view to TEST_SER_DEPL_1_MGR_Target;
