-- liquibase formatted sql

-- changeset scolameo:1694179951382-1
CREATE SEQUENCE SEQ_RM_PROZ_DOK_INFO_ID START WITH 316 MAXVALUE 9999999999 NOCACHE  ORDER;

-- changeset scolameo:1694179951382-2
CREATE SEQUENCE SEQ_RM_PROZ_ID START WITH 193 MAXVALUE 9999999999 NOCACHE  ORDER;

-- changeset scolameo:1694179951382-3
CREATE TABLE RM_ADRESSE (RM_ADRESSE_ID NUMBER NOT NULL, RM_ADRESSE_TYP NUMBER NOT NULL, RM_ADRESSE_CODE VARCHAR2(30 BYTE) NOT NULL, VERSCHLUESSELUNG_TYP NUMBER(3, 0), KOMPRIMIERUNG_TYP NUMBER(3, 0), KONVERTIERUNG_TYP NUMBER(3, 0), MUTDAT date, MUTIDE VARCHAR2(30 CHAR), EXPECTED_MIMETYPE VARCHAR2(2000 BYTE), CONSTRAINT RM_DOK_ADRESSE_PK PRIMARY KEY (RM_ADRESSE_ID, RM_ADRESSE_TYP)) TABLESPACE STUDADM;
COMMENT ON TABLE RM_ADRESSE IS 'Name: Dokument Platform Adresse Info.';
COMMENT ON COLUMN RM_ADRESSE.MUTDAT IS 'Name: Mutationsdatum
Beschreibung: Zeitstempel der letzten Mutation des Tabelleneintrags (INSERT oder UPDATE)

Wird von der Datenbank automatisch mitgeführt.';
COMMENT ON COLUMN RM_ADRESSE.MUTIDE IS 'Name: Mutations ID
Beschreibung: Benutzer, der die letzte Mutation des Tabelleneintrags gemacht hat.

Enthält DB-Benutzernamen und wird von der Datenbank automatisch mitgeführt.';
COMMENT ON COLUMN RM_ADRESSE.EXPECTED_MIMETYPE IS 'Name: Expected Mime-Type
Beschreibung: Liste von erwarteten Mime-Types mit '';'' Separator';

-- changeset scolameo:1694179951382-4
CREATE TABLE RM_ADRESSE_ARCHIV_OT (RM_ADRESSE_ID NUMBER(10, 0) NOT NULL, RM_ADRESSE_TYP NUMBER(3, 0) DEFAULT 2 NOT NULL, LOGISCHES_ARCHIV VARCHAR2(2 CHAR) NOT NULL, ARCHIVSERVER VARCHAR2(50 CHAR) NOT NULL, ARCHIVSERVER_STANDBY VARCHAR2(50 CHAR) NOT NULL, MUTDAT date, MUTIDE VARCHAR2(30 CHAR), CONSTRAINT PK_RM_ADRESSE_ARCHIV_OT PRIMARY KEY (RM_ADRESSE_ID, RM_ADRESSE_TYP)) TABLESPACE STUDADM;
COMMENT ON TABLE RM_ADRESSE_ARCHIV_OT IS 'Name:  OpenText Platform Adresse Indfo
Beschreibung:';
COMMENT ON COLUMN RM_ADRESSE_ARCHIV_OT.LOGISCHES_ARCHIV IS 'Name: Logisches Archiv
Beschreibung: Logischer Name des Opentext-Archivs. Opentext erlaubt maximal 2-stellige alphanumerische Namen.

Dies ist auch gleichzeitig der Primärschlüssel.';
COMMENT ON COLUMN RM_ADRESSE_ARCHIV_OT.ARCHIVSERVER IS 'Name: Archivserver
Beschreibung: Hostname inkl. Domain des Opentext-Archivservers.

Beispiel  ot-art1.ethz.ch';
COMMENT ON COLUMN RM_ADRESSE_ARCHIV_OT.ARCHIVSERVER_STANDBY IS 'Name: Archivserver Standby
Beschreibung: Hostname inkl. Domain des Remote Standby Opentext-Archivservers.
Falls der "normale" Archivserver nicht erreichbar ist, soll der Standby Server verwendet werden (automatische Umschaltung in der Applikation)

Beispiel  ot-art1.ethz.ch';
COMMENT ON COLUMN RM_ADRESSE_ARCHIV_OT.MUTDAT IS 'Name: Mutationsdatum
Beschreibung: Zeitstempel der letzten Mutation des Tabelleneintrags (INSERT oder UPDATE)

Wird von der Datenbank automatisch mitgeführt.';
COMMENT ON COLUMN RM_ADRESSE_ARCHIV_OT.MUTIDE IS 'Name: Mutations ID
Beschreibung: Benutzer, der die letzte Mutation des Tabelleneintrags gemacht hat.

Enthält DB-Benutzernamen und wird von der Datenbank automatisch mitgeführt.';

-- changeset scolameo:1694179951382-5
CREATE TABLE RM_ADRESSE_ARCHIV_OT_HIST (DB_OPERATION CHAR(1 BYTE), DELIDE VARCHAR2(30 BYTE), INS_TIME date, RM_ADRESSE_ID NUMBER(10, 0), RM_ADRESSE_TYP NUMBER(3, 0), LOGISCHES_ARCHIV VARCHAR2(2 BYTE), ARCHIVSERVER VARCHAR2(50 BYTE), ARCHIVSERVER_STANDBY VARCHAR2(50 BYTE), MUTDAT date, MUTIDE VARCHAR2(30 BYTE)) TABLESPACE STUDADM;

-- changeset scolameo:1694179951382-6
CREATE TABLE RM_ADRESSE_DISK_WL (RM_ADRESSE_ID NUMBER(10, 0) NOT NULL, RM_ADRESSE_TYP NUMBER(3, 0) DEFAULT 3 NOT NULL, DRIVE_MOUNT_PATH VARCHAR2(80 CHAR) NOT NULL, FOLDER_PATH VARCHAR2(80 CHAR) NOT NULL, MUTDAT date, MUTIDE VARCHAR2(30 CHAR), CONSTRAINT RM_ADRESSE_NASV1_PK PRIMARY KEY (RM_ADRESSE_ID, RM_ADRESSE_TYP)) TABLESPACE STUDADM;
COMMENT ON TABLE RM_ADRESSE_DISK_WL IS 'Name: WebLogic Disk Adresse Info';
COMMENT ON COLUMN RM_ADRESSE_DISK_WL.MUTDAT IS 'Name: Mutationsdatum
Beschreibung: Zeitstempel der letzten Mutation des Tabelleneintrags (INSERT oder UPDATE)

Wird von der Datenbank automatisch mitgeführt.';
COMMENT ON COLUMN RM_ADRESSE_DISK_WL.MUTIDE IS 'Name: Mutations ID
Beschreibung: Benutzer, der die letzte Mutation des Tabelleneintrags gemacht hat.

Enthält DB-Benutzernamen und wird von der Datenbank automatisch mitgeführt.';

-- changeset scolameo:1694179951382-7
CREATE TABLE RM_ADRESSE_DISK_WL_HIST (DB_OPERATION CHAR(1 BYTE), DELIDE VARCHAR2(30 BYTE), INS_TIME date, RM_ADRESSE_ID NUMBER(10, 0), RM_ADRESSE_TYP NUMBER(3, 0), DRIVE_MOUNT_PATH VARCHAR2(80 BYTE), FOLDER_PATH VARCHAR2(80 BYTE), MUTDAT date, MUTIDE VARCHAR2(30 BYTE)) TABLESPACE STUDADM;

-- changeset scolameo:1694179951382-8
CREATE TABLE RM_ADRESSE_HIST (DB_OPERATION CHAR(1 BYTE), DELIDE VARCHAR2(30 BYTE), INS_TIME date, RM_ADRESSE_ID NUMBER(10, 0), RM_ADRESSE_TYP NUMBER(10, 0), RM_ADRESSE_CODE VARCHAR2(30 BYTE), VERSCHLUESSELUNG_TYP NUMBER(3, 0), KOMPRIMIERUNG_TYP NUMBER(3, 0), KONVERTIERUNG_TYP NUMBER(3, 0), MUTDAT date, MUTIDE VARCHAR2(30 BYTE)) TABLESPACE STUDADM;

-- changeset scolameo:1694179951382-9
CREATE TABLE RM_ADRESSE_NAS (RM_ADRESSE_ID NUMBER(10, 0) NOT NULL, RM_ADRESSE_TYP NUMBER(3, 0) DEFAULT 4 NOT NULL, NETWORK_MOUNT_PATH VARCHAR2(80 CHAR) NOT NULL, FOLDER_PATH VARCHAR2(80 CHAR) NOT NULL, MUTDAT date, MUTIDE VARCHAR2(30 CHAR), CONSTRAINT RM_ADRESSE_ORAV1_PK PRIMARY KEY (RM_ADRESSE_ID, RM_ADRESSE_TYP)) TABLESPACE STUDADM;
COMMENT ON TABLE RM_ADRESSE_NAS IS 'Name: NAS Platform Adresse Indfo
Beschreibung:';
COMMENT ON COLUMN RM_ADRESSE_NAS.MUTDAT IS 'Name: Mutationsdatum
Beschreibung: Zeitstempel der letzten Mutation des Tabelleneintrags (INSERT oder UPDATE)

Wird von der Datenbank automatisch mitgeführt.';
COMMENT ON COLUMN RM_ADRESSE_NAS.MUTIDE IS 'Name: Mutations ID
Beschreibung: Benutzer, der die letzte Mutation des Tabelleneintrags gemacht hat.

Enthält DB-Benutzernamen und wird von der Datenbank automatisch mitgeführt.';

-- changeset scolameo:1694179951382-10
CREATE TABLE RM_ADRESSE_NAS_HIST (DB_OPERATION CHAR(1 BYTE), DELIDE VARCHAR2(30 BYTE), INS_TIME date, RM_ADRESSE_ID NUMBER(10, 0), RM_ADRESSE_TYP NUMBER(3, 0), NETWORK_MOUNT_PATH VARCHAR2(80 BYTE), FOLDER_PATH VARCHAR2(80 BYTE), MUTDAT date, MUTIDE VARCHAR2(30 BYTE)) TABLESPACE STUDADM;

-- changeset scolameo:1694179951382-11
CREATE TABLE RM_ADRESSE_ORA (RM_ADRESSE_ID NUMBER(10, 0) NOT NULL, RM_ADRESSE_TYP NUMBER(3, 0) DEFAULT 1 NOT NULL, DBMS_CONECTION_POOL_ALIAS VARCHAR2(50 CHAR) NOT NULL, SCHEMA_NAME VARCHAR2(50 CHAR) NOT NULL, TABLE_NAME VARCHAR2(50 CHAR) NOT NULL, TABLE_PK_SEQUENCE_NAME NVARCHAR2(50), BLOB_COL_NAME VARCHAR2(50 CHAR) NOT NULL, MUTDAT date, MUTIDE VARCHAR2(30 CHAR), MIMETYPE_COL_NAME VARCHAR2(30 BYTE), PK_COL_NAME VARCHAR2(30 BYTE), DBMS_HOST VARCHAR2(200 BYTE), DBMS_PORT VARCHAR2(200 BYTE), CONSTRAINT RM_DOK_ADRESSEV1_PK PRIMARY KEY (RM_ADRESSE_ID, RM_ADRESSE_TYP)) TABLESPACE STUDADM;
COMMENT ON TABLE RM_ADRESSE_ORA IS 'Name: RM Adresse Oracle Platform
Beschreibung: Beschreibt Verbindung Info für RM Adresse Oracle Platform.';
COMMENT ON COLUMN RM_ADRESSE_ORA.DBMS_CONECTION_POOL_ALIAS IS 'Name: Dbms Conection Pool Alias';
COMMENT ON COLUMN RM_ADRESSE_ORA.MUTDAT IS 'Name: Mutationsdatum
Beschreibung: Zeitstempel der letzten Mutation des Tabelleneintrags (INSERT oder UPDATE)

Wird von der Datenbank automatisch mitgeführt.';
COMMENT ON COLUMN RM_ADRESSE_ORA.MUTIDE IS 'Name: Mutations ID
Beschreibung: Benutzer, der die letzte Mutation des Tabelleneintrags gemacht hat.

Enthält DB-Benutzernamen und wird von der Datenbank automatisch mitgeführt.';
COMMENT ON COLUMN RM_ADRESSE_ORA.PK_COL_NAME IS '_';

-- changeset scolameo:1694179951382-12
CREATE TABLE RM_ADRESSE_ORA_HIST (DB_OPERATION CHAR(1 BYTE), DELIDE VARCHAR2(30 BYTE), INS_TIME date, RM_ADRESSE_ID NUMBER(10, 0), RM_ADRESSE_TYP NUMBER(3, 0), DBMS_CONECTION_POOL_ALIAS VARCHAR2(50 BYTE), SCHEMA_NAME VARCHAR2(50 BYTE), TABLE_NAME VARCHAR2(50 BYTE), TABLE_PK_SEQUENCE_NAME NVARCHAR2(50), BLOB_COL_NAME VARCHAR2(50 BYTE), MIMETYPE_COL_NAME VARCHAR2(30 BYTE), PK_COL_NAME VARCHAR2(30 BYTE), DBMS_HOST VARCHAR2(200 BYTE), DBMS_PORT VARCHAR2(200 BYTE), MUTDAT date, MUTIDE VARCHAR2(30 BYTE)) TABLESPACE STUDADM;

-- changeset scolameo:1694179951382-13
CREATE TABLE RM_PROZ (RM_PROZ_ID NUMBER(10, 0), PROZ_TYPE_ID NUMBER(3, 0) DEFAULT 0, PROZ_DOK_ADRESSE_QUELLE NUMBER(10, 0), PROZ_DOK_ADRESSE_ZIEL NUMBER(10, 0), PROZ_STATUS_TS TIMESTAMP(6), PROZ_STATUS_ID NUMBER(3, 0) DEFAULT 0, PROZ_FEHLERZAHL NUMBER(4, 0) DEFAULT 0, PROZ_FEHLER_TS TIMESTAMP(6), PROZ_FEHLER_LOG VARCHAR2(4000 CHAR), DOK_HASH_CHECK_OK NUMBER(1, 0), DOK_HASH_CHECK_OK_TS TIMESTAMP(6), ZIEL_DOK_SEGMENT VARCHAR2(4000 CHAR), MUTDAT date, MUTIDE VARCHAR2(30 CHAR)) TABLESPACE STUDADM;
COMMENT ON TABLE RM_PROZ IS 'Name: RM Prozess
Beschreibung: RM Dokument Prozess';
COMMENT ON COLUMN RM_PROZ.RM_PROZ_ID IS 'Name: RM Dokument Prozess ID
Beschreibung: Wird mit Sequenz SEQ_rm_proz_ID verwaltet';
COMMENT ON COLUMN RM_PROZ.PROZ_TYPE_ID IS 'Name: RM Prozess Typ
Beschreibung: FK-Referenz zu RM-Prozess-Typ Tabelle';
COMMENT ON COLUMN RM_PROZ.PROZ_DOK_ADRESSE_QUELLE IS 'Name: Proz Dok Adresse Quelle
Beschreibung: FK-Referenz Quell-Dokument-Lokator';
COMMENT ON COLUMN RM_PROZ.PROZ_DOK_ADRESSE_ZIEL IS 'Name: Proz Dok Adresse Quelle
Beschreibung: FK-Referenz Ziel-Dokument-Lokator';
COMMENT ON COLUMN RM_PROZ.PROZ_STATUS_TS IS 'Name: RM Proz Status  TS
Beschreibung: Timestamp zu Proz Status.';
COMMENT ON COLUMN RM_PROZ.PROZ_STATUS_ID IS 'Name: RM Prozess Status
Beschreibung: FK-Referenz zu RM-Prozess-Status Tabelle';
COMMENT ON COLUMN RM_PROZ.PROZ_FEHLERZAHL IS 'Name: RM-Proz Fehlerzahl
Beschreibung: Zähler für fehlerhafte Versuche im RM-Synch-Prozess.

Wird jedesmal erhöht, wenn die Archivierung mit Fehler abbricht. Ist eine vergegebene Limite ereicht, wird keine weiter Prozessierung versucht.

Defaultwert=Anfangswert ist 0.';
COMMENT ON COLUMN RM_PROZ.PROZ_FEHLER_TS IS 'Name: RM Proz Fehler TS
Beschreibung: Timestamp zu "Archproz Fehlerzahl".

Ist NULL, wenn beim Prozess kein Fehler auftrat oder kein Prozess durchgeführt wurde.';
COMMENT ON COLUMN RM_PROZ.PROZ_FEHLER_LOG IS 'Name: RM Proz Fehler Log
Beschreibung: Log mit Details zur Fehler';
COMMENT ON COLUMN RM_PROZ.DOK_HASH_CHECK_OK IS 'Name: Dok Hash Check Ok
Beschreibung: Sind Ziel- und Quell-Dokument Hash gleich';
COMMENT ON COLUMN RM_PROZ.DOK_HASH_CHECK_OK_TS IS 'Name: Dok Hash Check Ok TS
Beschreibung:';
COMMENT ON COLUMN RM_PROZ.ZIEL_DOK_SEGMENT IS 'Name: ziel dok SEGMENT
Beschreibung:';
COMMENT ON COLUMN RM_PROZ.MUTDAT IS 'Name: Mutationsdatum
Beschreibung: Zeitstempel der letzten Mutation des Tabelleneintrags (INSERT oder UPDATE)

Wird von der Datenbank automatisch mitgeführt.';
COMMENT ON COLUMN RM_PROZ.MUTIDE IS 'Name: Mutations ID
Beschreibung: Benutzer, der die letzte Mutation des Tabelleneintrags gemacht hat.

Enthält DB-Benutzernamen und wird von der Datenbank automatisch mitgeführt.';

-- changeset scolameo:1694179951382-14
CREATE TABLE RM_PROZ_COPY (RM_PROZ_ID NUMBER(10, 0) NOT NULL, PROZ_TYPE_ID NUMBER(3, 0) DEFAULT 0 NOT NULL, PROZ_DOK_ADRESSE_QUELLE NUMBER(10, 0), PROZ_DOK_ADRESSE_ZIEL NUMBER(10, 0), PROZ_STATUS_TS TIMESTAMP(6), PROZ_STATUS_ID NUMBER(3, 0) DEFAULT 0 NOT NULL, PROZ_FEHLERZAHL NUMBER(4, 0) DEFAULT 0 NOT NULL, PROZ_FEHLER_TS TIMESTAMP(6), PROZ_FEHLER_LOG VARCHAR2(4000 CHAR), DOK_HASH_CHECK_OK NUMBER(1, 0), DOK_HASH_CHECK_OK_TS TIMESTAMP(6), ZIEL_DOK_SEGMENT VARCHAR2(4000 CHAR), MUTDAT date, MUTIDE VARCHAR2(30 CHAR), CONSTRAINT RM_PROZ_PK PRIMARY KEY (RM_PROZ_ID)) TABLESPACE STUDADM;
COMMENT ON TABLE RM_PROZ_COPY IS 'Name: RM Prozess
Beschreibung: RM Dokument Prozess';
COMMENT ON COLUMN RM_PROZ_COPY.RM_PROZ_ID IS 'Name: RM Dokument Prozess ID
Beschreibung: Wird mit Sequenz SEQ_rm_proz_ID verwaltet';
COMMENT ON COLUMN RM_PROZ_COPY.PROZ_TYPE_ID IS 'Name: RM Prozess Typ
Beschreibung: FK-Referenz zu RM-Prozess-Typ Tabelle';
COMMENT ON COLUMN RM_PROZ_COPY.PROZ_DOK_ADRESSE_QUELLE IS 'Name: Proz Dok Adresse Quelle
Beschreibung: FK-Referenz Quell-Dokument-Lokator';
COMMENT ON COLUMN RM_PROZ_COPY.PROZ_DOK_ADRESSE_ZIEL IS 'Name: Proz Dok Adresse Quelle
Beschreibung: FK-Referenz Ziel-Dokument-Lokator';
COMMENT ON COLUMN RM_PROZ_COPY.PROZ_STATUS_TS IS 'Name: RM Proz Status  TS
Beschreibung: Timestamp zu Proz Status.';
COMMENT ON COLUMN RM_PROZ_COPY.PROZ_STATUS_ID IS 'Name: RM Prozess Status
Beschreibung: FK-Referenz zu RM-Prozess-Status Tabelle';
COMMENT ON COLUMN RM_PROZ_COPY.PROZ_FEHLERZAHL IS 'Name: RM-Proz Fehlerzahl
Beschreibung: Zähler für fehlerhafte Versuche im RM-Synch-Prozess.

Wird jedesmal erhöht, wenn die Archivierung mit Fehler abbricht. Ist eine vergegebene Limite ereicht, wird keine weiter Prozessierung versucht.

Defaultwert=Anfangswert ist 0.';
COMMENT ON COLUMN RM_PROZ_COPY.PROZ_FEHLER_TS IS 'Name: RM Proz Fehler TS
Beschreibung: Timestamp zu "Archproz Fehlerzahl".

Ist NULL, wenn beim Prozess kein Fehler auftrat oder kein Prozess durchgeführt wurde.';
COMMENT ON COLUMN RM_PROZ_COPY.PROZ_FEHLER_LOG IS 'Name: RM Proz Fehler Log
Beschreibung: Log mit Details zur Fehler';
COMMENT ON COLUMN RM_PROZ_COPY.DOK_HASH_CHECK_OK IS 'Name: Dok Hash Check Ok
Beschreibung: Sind Ziel- und Quell-Dokument Hash gleich';
COMMENT ON COLUMN RM_PROZ_COPY.DOK_HASH_CHECK_OK_TS IS 'Name: Dok Hash Check Ok TS
Beschreibung:';
COMMENT ON COLUMN RM_PROZ_COPY.ZIEL_DOK_SEGMENT IS 'Name: ziel dok SEGMENT
Beschreibung:';
COMMENT ON COLUMN RM_PROZ_COPY.MUTDAT IS 'Name: Mutationsdatum
Beschreibung: Zeitstempel der letzten Mutation des Tabelleneintrags (INSERT oder UPDATE)

Wird von der Datenbank automatisch mitgeführt.';
COMMENT ON COLUMN RM_PROZ_COPY.MUTIDE IS 'Name: Mutations ID
Beschreibung: Benutzer, der die letzte Mutation des Tabelleneintrags gemacht hat.

Enthält DB-Benutzernamen und wird von der Datenbank automatisch mitgeführt.';

-- changeset scolameo:1694179951382-15
CREATE TABLE RM_PROZ_DOK_INFO (RM_PROZ_DOK_INFO_ID NUMBER(10, 0), RM_ADRESSE_ID NUMBER(10, 0), RM_ADRESSE_TYP NUMBER(3, 0), DOK_ID VARCHAR2(100 CHAR), MIMETYPE VARCHAR2(100 CHAR), DATEINAME VARCHAR2(100 CHAR), ANZ_BYTES NUMBER(12, 0), DOK_HASH VARCHAR2(100 CHAR), MUTDAT date, MUTIDE VARCHAR2(30 CHAR), BEMERKUNG VARCHAR2(200 CHAR), REF_METADATA_DOK_ID NUMBER(12, 0)) TABLESPACE STUDADM;
COMMENT ON TABLE RM_PROZ_DOK_INFO IS 'Dokument Locator


Name: RM Dokument
Beschreibung: RM Dokument enthält die Metadaten zu einem Dokument das schon archiviert ist oder noch archiviert werden soll. Hierbei bedeutet archiviert, dass das Dokument in digitaler Form - normalerweise im PDF/A Format - im RM-Archiv (an der ETH betriebenes Opentext Archivsystem für den Lehrbetrieb) gespeichert ist. Es geht hier nicht um die Archivierung im Bundesarchiv.

In der Prodspez LISETHpro sind diverse Dokumentenmerkmale aufgeführt, die hier nicht implementiert sind, und zwar aus folgenden Gründen:

Datenschutzstufe:
nicht nötig, da alle Dokumente sowieso dieselbe Stufe "ja=besonders schützenswert" haben. Ein Wert, der überall gleich ist muss nicht gespeichert werden.

Zugriffsberechtigung: ist Benutzerabhänig und kann somit nicht beim RM Dokument gespeichert werden. Zugriffsberechtigung wird pro Benutzer/Person rollenbasiert und abhängig von der Dokumentenart bestimmt. Siehe separate Tabellen RM_ROLLE, RM_ROLLE_PERSON UND RM_ROLLENZUORDNUNG.

Verweis:
keine Felder auf Vorrat. Bisher keine sinnvolle Verwendung dokumentiert.

Öffentlichkeitsstatus:
nicht nötig. Ein Wert, der überall gleich ist muss nicht gespeichert werden.

Bearbeitungsstatus:
unnötig, da anderweitig oder gar nicht feststellbar: "Metadaten ohne PDF" und "Dokument vollständig" sind erkennbar an Existenz der Dok-DokumentId. "PDF in Bearbeitung" ist entweder an der Tempdok-DokumentId erkennbar, oder kann nicht von "Metadaten ohne PDF unterschieden werden, wenn Scanning via Pipeline erfolgt (ausser wenn dieser Wert manuell gesetzt wird, aber so wie ich die Lage beurteile, will niemand manuell diesen Status setzen...).';
COMMENT ON COLUMN RM_PROZ_DOK_INFO.MIMETYPE IS 'Name: Mimetype
Beschreibung: Mimetype des Dokuments im Originalformat).';
COMMENT ON COLUMN RM_PROZ_DOK_INFO.DATEINAME IS 'Name: Dateiname
Beschreibung: Dateiname des eingestellten Dokuments';
COMMENT ON COLUMN RM_PROZ_DOK_INFO.MUTDAT IS 'Name: Mutationsdatum
Beschreibung: Zeitstempel der letzten Mutation des Tabelleneintrags (INSERT oder UPDATE)

Wird von der Datenbank automatisch mitgeführt.';
COMMENT ON COLUMN RM_PROZ_DOK_INFO.MUTIDE IS 'Name: Mutations ID
Beschreibung: Benutzer, der die letzte Mutation des Tabelleneintrags gemacht hat.

Enthält DB-Benutzernamen und wird von der Datenbank automatisch mitgeführt.';

-- changeset scolameo:1694179951382-16
CREATE TABLE RM_PROZ_DOK_INFO_COPY (RM_PROZ_DOK_INFO_ID NUMBER(10, 0) NOT NULL, RM_ADRESSE_ID NUMBER(10, 0) NOT NULL, RM_ADRESSE_TYP NUMBER(3, 0) NOT NULL, DOK_ID VARCHAR2(100 CHAR), MIMETYPE VARCHAR2(100 CHAR), DATEINAME VARCHAR2(100 CHAR), ANZ_BYTES NUMBER(12, 0), DOK_HASH VARCHAR2(100 CHAR), MUTDAT date, MUTIDE VARCHAR2(30 CHAR), BEMERKUNG VARCHAR2(200 CHAR), CONSTRAINT RM_PROZ_DOK_INFO_PK PRIMARY KEY (RM_PROZ_DOK_INFO_ID)) TABLESPACE STUDADM;
COMMENT ON TABLE RM_PROZ_DOK_INFO_COPY IS 'Dokument Locator


Name: RM Dokument
Beschreibung: RM Dokument enthält die Metadaten zu einem Dokument das schon archiviert ist oder noch archiviert werden soll. Hierbei bedeutet archiviert, dass das Dokument in digitaler Form - normalerweise im PDF/A Format - im RM-Archiv (an der ETH betriebenes Opentext Archivsystem für den Lehrbetrieb) gespeichert ist. Es geht hier nicht um die Archivierung im Bundesarchiv.

In der Prodspez LISETHpro sind diverse Dokumentenmerkmale aufgeführt, die hier nicht implementiert sind, und zwar aus folgenden Gründen:

Datenschutzstufe:
nicht nötig, da alle Dokumente sowieso dieselbe Stufe "ja=besonders schützenswert" haben. Ein Wert, der überall gleich ist muss nicht gespeichert werden.

Zugriffsberechtigung: ist Benutzerabhänig und kann somit nicht beim RM Dokument gespeichert werden. Zugriffsberechtigung wird pro Benutzer/Person rollenbasiert und abhängig von der Dokumentenart bestimmt. Siehe separate Tabellen RM_ROLLE, RM_ROLLE_PERSON UND RM_ROLLENZUORDNUNG.

Verweis:
keine Felder auf Vorrat. Bisher keine sinnvolle Verwendung dokumentiert.

Öffentlichkeitsstatus:
nicht nötig. Ein Wert, der überall gleich ist muss nicht gespeichert werden.

Bearbeitungsstatus:
unnötig, da anderweitig oder gar nicht feststellbar: "Metadaten ohne PDF" und "Dokument vollständig" sind erkennbar an Existenz der Dok-DokumentId. "PDF in Bearbeitung" ist entweder an der Tempdok-DokumentId erkennbar, oder kann nicht von "Metadaten ohne PDF unterschieden werden, wenn Scanning via Pipeline erfolgt (ausser wenn dieser Wert manuell gesetzt wird, aber so wie ich die Lage beurteile, will niemand manuell diesen Status setzen...).';
COMMENT ON COLUMN RM_PROZ_DOK_INFO_COPY.MIMETYPE IS 'Name: Mimetype
Beschreibung: Mimetype des Dokuments im Originalformat).';
COMMENT ON COLUMN RM_PROZ_DOK_INFO_COPY.DATEINAME IS 'Name: Dateiname
Beschreibung: Dateiname des eingestellten Dokuments';
COMMENT ON COLUMN RM_PROZ_DOK_INFO_COPY.MUTDAT IS 'Name: Mutationsdatum
Beschreibung: Zeitstempel der letzten Mutation des Tabelleneintrags (INSERT oder UPDATE)

Wird von der Datenbank automatisch mitgeführt.';
COMMENT ON COLUMN RM_PROZ_DOK_INFO_COPY.MUTIDE IS 'Name: Mutations ID
Beschreibung: Benutzer, der die letzte Mutation des Tabelleneintrags gemacht hat.

Enthält DB-Benutzernamen und wird von der Datenbank automatisch mitgeführt.';

-- changeset scolameo:1694179951382-17
CREATE TABLE RM_PROZ_DOK_INFO_HIST (DB_OPERATION CHAR(1 BYTE), DELIDE VARCHAR2(30 BYTE), INS_TIME date, RM_PROZ_DOK_INFO_ID NUMBER(10, 0), RM_ADRESSE_ID NUMBER(10, 0), RM_ADRESSE_TYP NUMBER(3, 0), DOK_ID VARCHAR2(100 BYTE), MIMETYPE VARCHAR2(100 BYTE), DATEINAME VARCHAR2(100 BYTE), ANZ_BYTES NUMBER(12, 0), DOK_HASH VARCHAR2(100 BYTE), BEMERKUNG VARCHAR2(200 BYTE), MUTDAT date, MUTIDE VARCHAR2(30 BYTE)) TABLESPACE STUDADM;

-- changeset scolameo:1694179951382-18
CREATE TABLE RM_PROZ_HIST (DB_OPERATION CHAR(1 BYTE), DELIDE VARCHAR2(30 BYTE), INS_TIME date, RM_PROZ_ID NUMBER(10, 0), PROZ_TYPE_ID NUMBER(3, 0), PROZ_DOK_ADRESSE_QUELLE NUMBER(10, 0), PROZ_DOK_ADRESSE_ZIEL NUMBER(10, 0), PROZ_STATUS_TS TIMESTAMP(6), PROZ_STATUS_ID NUMBER(3, 0), PROZ_FEHLERZAHL NUMBER(4, 0), PROZ_FEHLER_TS TIMESTAMP(6), PROZ_FEHLER_LOG VARCHAR2(4000 BYTE), DOK_HASH_CHECK_OK NUMBER(1, 0), DOK_HASH_CHECK_OK_TS TIMESTAMP(6), ZIEL_DOK_SEGMENT VARCHAR2(4000 BYTE), MUTDAT date, MUTIDE VARCHAR2(30 BYTE)) TABLESPACE STUDADM;

-- changeset scolameo:1694179951382-19
CREATE TABLE RM_PROZ_STATUS (RM_PROZ_STATUS_ID NUMBER(10, 0) NOT NULL, STATUS_NAME VARCHAR2(80 BYTE) DEFAULT '0' NOT NULL, STATUS_CODE NUMBER(3, 0) DEFAULT 0 NOT NULL, IST_IN_BEARBEITUNG NUMBER(1, 0) NOT NULL, IST_ERFOLGREICH_BEENDET NUMBER(1, 0) DEFAULT 0 NOT NULL, HAT_QUELL_VERBINDUNG_FEHLER NUMBER(1, 0) DEFAULT 0 NOT NULL, HAT_QUELL_ADRESSE_FEHLER NUMBER(1, 0) DEFAULT 0 NOT NULL, HAT_QUELL_DOK_ID_FEHLER NUMBER(1, 0) DEFAULT 0 NOT NULL, HAT_ZIEL_VERBINDUNG_FEHLER NUMBER(1, 0) DEFAULT 0 NOT NULL, HAT_ZIEL_ADRESSE_FEHLER NUMBER(1, 0) DEFAULT 0 NOT NULL, HAT_ZIEL_DOK_ID_FEHLER NUMBER(1, 0) DEFAULT 0 NOT NULL, MUTDAT date, MUTIDE VARCHAR2(30 CHAR), CONSTRAINT RM_PROZ_STATUS_PK PRIMARY KEY (RM_PROZ_STATUS_ID)) TABLESPACE STUDADM;
COMMENT ON TABLE RM_PROZ_STATUS IS 'Name: RM Prozess Status';
COMMENT ON COLUMN RM_PROZ_STATUS.RM_PROZ_STATUS_ID IS 'Name: RM Proz Status  Id
Beschreibung:';
COMMENT ON COLUMN RM_PROZ_STATUS.STATUS_NAME IS 'Name: Prozess Status Name
Beschreibung:';
COMMENT ON COLUMN RM_PROZ_STATUS.STATUS_CODE IS 'Name: Status Code
Beschreibung: für externe Referenzierung';
COMMENT ON COLUMN RM_PROZ_STATUS.IST_IN_BEARBEITUNG IS 'Name: Ist Prozess in Ausführung
Beschreibung: 
1: wahr 
0: falsch';
COMMENT ON COLUMN RM_PROZ_STATUS.IST_ERFOLGREICH_BEENDET IS 'Name: Ist Prozess erfolgreich beendet
Beschreibung: 
1: wahr 
0: falsch';
COMMENT ON COLUMN RM_PROZ_STATUS.HAT_QUELL_VERBINDUNG_FEHLER IS 'Name: Hat Fehler bei der Quellverbindung
Beschreibung:
1: wahr 
0: falsch';
COMMENT ON COLUMN RM_PROZ_STATUS.HAT_QUELL_ADRESSE_FEHLER IS 'Name: Hat Fehler bei der Quelladresset
Beschreibung:
1: wahr 
0: falsch';
COMMENT ON COLUMN RM_PROZ_STATUS.HAT_QUELL_DOK_ID_FEHLER IS 'Name: Hat Fehler bei der Quellen-Dokument-ID
Beschreibung:
1: wahr 
0: falsch';
COMMENT ON COLUMN RM_PROZ_STATUS.HAT_ZIEL_VERBINDUNG_FEHLER IS 'Name: Hat Fehler bei der Zielverbindung
Beschreibung:
1: wahr 
0: falsch';
COMMENT ON COLUMN RM_PROZ_STATUS.HAT_ZIEL_ADRESSE_FEHLER IS 'Name: Hat Fehler bei der Zieladresse
Beschreibung:
1: wahr 
0: falsch';
COMMENT ON COLUMN RM_PROZ_STATUS.HAT_ZIEL_DOK_ID_FEHLER IS 'Name: Hat Fehler bei der Ziel-Dokument-ID
Beschreibung:
1: wahr 
0: falsch';
COMMENT ON COLUMN RM_PROZ_STATUS.MUTDAT IS 'Name: Mutationsdatum
Beschreibung: Zeitstempel der letzten Mutation des Tabelleneintrags (INSERT oder UPDATE)

Wird von der Datenbank automatisch mitgeführt.';
COMMENT ON COLUMN RM_PROZ_STATUS.MUTIDE IS 'Name: Mutations ID
Beschreibung: Benutzer, der die letzte Mutation des Tabelleneintrags gemacht hat.

Enthält DB-Benutzernamen und wird von der Datenbank automatisch mitgeführt.';

-- changeset scolameo:1694179951382-20
CREATE TABLE RM_PROZ_STATUS_HIST (DB_OPERATION CHAR(1 BYTE), DELIDE VARCHAR2(30 BYTE), INS_TIME date, RM_PROZ_STATUS_ID NUMBER(10, 0), STATUS_NAME VARCHAR2(80 BYTE), STATUS_CODE NUMBER(3, 0), IST_IN_BEARBEITUNG NUMBER(1, 0), IST_ERFOLGREICH_BEENDET NUMBER(1, 0), HAT_QUELL_VERBINDUNG_FEHLER NUMBER(1, 0), HAT_QUELL_ADRESSE_FEHLER NUMBER(1, 0), HAT_QUELL_DOK_ID_FEHLER NUMBER(1, 0), HAT_ZIEL_VERBINDUNG_FEHLER NUMBER(1, 0), HAT_ZIEL_ADRESSE_FEHLER NUMBER(1, 0), HAT_ZIEL_DOK_ID_FEHLER NUMBER(1, 0), MUTDAT date, MUTIDE VARCHAR2(30 BYTE)) TABLESPACE STUDADM;

-- changeset scolameo:1694179951382-21
CREATE TABLE RM_PROZ_TYPE (RM_PROZ_TYPE_ID NUMBER(10, 0) NOT NULL, PROZESS_TYP_NAME VARCHAR2(80 BYTE) DEFAULT '0' NOT NULL, PROZESS_TYP_CODE NUMBER(3, 0) DEFAULT 0 NOT NULL, SYNC_MOVE_DOC NUMBER(1, 0) DEFAULT 0 NOT NULL, SYNC_COPY_DOC NUMBER(1, 0) DEFAULT 0 NOT NULL, SYNC_KEEP_TARGET_OLD_DOC NUMBER(1, 0) NOT NULL, SYNC_CHECK_SOURCE_TARGET_DOC NUMBER(1, 0) DEFAULT 0 NOT NULL, SYNC_ENCHRYPT_TARGET_DOC NUMBER(1, 0) DEFAULT 0 NOT NULL, SYNC_COMPRESS_TARGET_DOC NUMBER(1, 0) DEFAULT 0 NOT NULL, SYNC_FORMAT_TARGET_DOC NUMBER(1, 0) DEFAULT 0 NOT NULL, VIRUSSCAN_TARGET_DOC NUMBER(1, 0) DEFAULT 0 NOT NULL, RETENTION_DELETE_TARGET_DOC NUMBER(1, 0) DEFAULT 0 NOT NULL, MUTDAT date, MUTIDE VARCHAR2(30 CHAR), SYNCH_WATERMARK_TARGET_DOC NUMBER(1, 0) DEFAULT 0 NOT NULL, SYNCH_WATERMARK_TEXT VARCHAR2(80 BYTE), CONSTRAINT RM_PROZ_TYPE_PK PRIMARY KEY (RM_PROZ_TYPE_ID)) TABLESPACE STUDADM;
COMMENT ON TABLE RM_PROZ_TYPE IS 'Name: RM Prozess Typ
Beschreibung: .';
COMMENT ON COLUMN RM_PROZ_TYPE.RM_PROZ_TYPE_ID IS 'Name: Prozess-Typ-Id
Beschreibung:';
COMMENT ON COLUMN RM_PROZ_TYPE.PROZESS_TYP_NAME IS 'Name: Prozess-Typ-Name
Beschreibung:';
COMMENT ON COLUMN RM_PROZ_TYPE.PROZESS_TYP_CODE IS 'Name: Prozess-Typ-Code
Beschreibung: für externe Referenzierung';
COMMENT ON COLUMN RM_PROZ_TYPE.SYNC_MOVE_DOC IS 'Name: Sync-Move-Doc
Beschreibung:
1: wahr 
0: falsch';
COMMENT ON COLUMN RM_PROZ_TYPE.SYNC_COPY_DOC IS 'Name: Sync-Copy-Doc
Beschreibung:
1: wahr 
0: falsch';
COMMENT ON COLUMN RM_PROZ_TYPE.SYNC_KEEP_TARGET_OLD_DOC IS 'Name: Sync-Keep-Target-Old-Doc
Beschreibung:
1: wahr 
0: falsch';
COMMENT ON COLUMN RM_PROZ_TYPE.SYNC_CHECK_SOURCE_TARGET_DOC IS 'Name: Sync-Check-Source-Target-Doc
Beschreibung:
1: wahr 
0: falsch';
COMMENT ON COLUMN RM_PROZ_TYPE.SYNC_ENCHRYPT_TARGET_DOC IS 'Name: Sync-Enchrypt-Target-Doc
Beschreibung:
1: wahr 
0: falsch';
COMMENT ON COLUMN RM_PROZ_TYPE.SYNC_COMPRESS_TARGET_DOC IS 'Name: Sync-Compress-Target-Doc
Beschreibung:
1: wahr 
0: falsch';
COMMENT ON COLUMN RM_PROZ_TYPE.SYNC_FORMAT_TARGET_DOC IS 'Name: Sync-Format-Target-Doc
Beschreibung:
1: wahr 
0: falsch';
COMMENT ON COLUMN RM_PROZ_TYPE.VIRUSSCAN_TARGET_DOC IS 'Name: Virusscan-Target-Doc
Beschreibung:
1: wahr 
0: falsch';
COMMENT ON COLUMN RM_PROZ_TYPE.RETENTION_DELETE_TARGET_DOC IS 'Name: Retention-Delete-Target-Doc
Beschreibung:
1: wahr 
0: falsch';
COMMENT ON COLUMN RM_PROZ_TYPE.MUTDAT IS 'Name: Mutationsdatum
Beschreibung: Zeitstempel der letzten Mutation des Tabelleneintrags (INSERT oder UPDATE)

Wird von der Datenbank automatisch mitgeführt.';
COMMENT ON COLUMN RM_PROZ_TYPE.MUTIDE IS 'Name: Mutations ID
Beschreibung: Benutzer, der die letzte Mutation des Tabelleneintrags gemacht hat.

Enthält DB-Benutzernamen und wird von der Datenbank automatisch mitgeführt.';

-- changeset scolameo:1694179951382-22
CREATE TABLE RM_PROZ_TYPE_HIST (DB_OPERATION CHAR(1 BYTE), DELIDE VARCHAR2(30 BYTE), INS_TIME date, RM_PROZ_TYPE_ID NUMBER(10, 0), PROZESS_TYP_NAME VARCHAR2(80 BYTE), PROZESS_TYP_CODE NUMBER(3, 0), SYNC_MOVE_DOC NUMBER(1, 0), SYNC_COPY_DOC NUMBER(1, 0), SYNC_KEEP_TARGET_OLD_DOC NUMBER(1, 0), SYNC_CHECK_SOURCE_TARGET_DOC NUMBER(1, 0), SYNC_ENCHRYPT_TARGET_DOC NUMBER(1, 0), SYNC_COMPRESS_TARGET_DOC NUMBER(1, 0), SYNC_FORMAT_TARGET_DOC NUMBER(1, 0), VIRUSSCAN_TARGET_DOC NUMBER(1, 0), RETENTION_DELETE_TARGET_DOC NUMBER(1, 0), MUTDAT date, MUTIDE VARCHAR2(30 BYTE)) TABLESPACE STUDADM;

-- changeset scolameo:1694179951382-23
ALTER TABLE RM_ADRESSE_ARCHIV_OT ADD CONSTRAINT RM_ADRESSE_ARC_OPT_RM_ADR_FK FOREIGN KEY (RM_ADRESSE_ID, RM_ADRESSE_TYP) REFERENCES RM_ADRESSE (RM_ADRESSE_ID, RM_ADRESSE_TYP);

-- changeset scolameo:1694179951382-24
ALTER TABLE RM_ADRESSE_NAS ADD CONSTRAINT RM_ADRESSE_NAS_RM_ADR_FK FOREIGN KEY (RM_ADRESSE_ID, RM_ADRESSE_TYP) REFERENCES RM_ADRESSE (RM_ADRESSE_ID, RM_ADRESSE_TYP);

-- changeset scolameo:1694179951382-25
ALTER TABLE RM_ADRESSE_ORA ADD CONSTRAINT RM_ADRESSE_ORA_RM_ADR_FK FOREIGN KEY (RM_ADRESSE_ID, RM_ADRESSE_TYP) REFERENCES RM_ADRESSE (RM_ADRESSE_ID, RM_ADRESSE_TYP);

-- changeset scolameo:1694179951382-26
ALTER TABLE RM_ADRESSE_DISK_WL ADD CONSTRAINT RM_ADRESSE_WL_RM_ADR_FK FOREIGN KEY (RM_ADRESSE_ID, RM_ADRESSE_TYP) REFERENCES RM_ADRESSE (RM_ADRESSE_ID, RM_ADRESSE_TYP);

-- changeset scolameo:1694179951382-27
ALTER TABLE RM_PROZ_DOK_INFO_COPY ADD CONSTRAINT RM_PROZ_DOK_INFO_RM_ADR_FK FOREIGN KEY (RM_ADRESSE_ID, RM_ADRESSE_TYP) REFERENCES RM_ADRESSE (RM_ADRESSE_ID, RM_ADRESSE_TYP);

-- changeset scolameo:1694179951382-28
ALTER TABLE RM_PROZ_COPY ADD CONSTRAINT RM_PROZ_QUELLE_RM_PROZ_DOK_FK FOREIGN KEY (PROZ_DOK_ADRESSE_QUELLE) REFERENCES RM_PROZ_DOK_INFO_COPY (RM_PROZ_DOK_INFO_ID);

-- changeset scolameo:1694179951382-29
ALTER TABLE RM_PROZ_COPY ADD CONSTRAINT RM_PROZ_RM_PROZ_STATUS_FK FOREIGN KEY (PROZ_STATUS_ID) REFERENCES RM_PROZ_STATUS (RM_PROZ_STATUS_ID);

-- changeset scolameo:1694179951382-30
ALTER TABLE RM_PROZ_COPY ADD CONSTRAINT RM_PROZ_RM_PROZ_TYPE_FK FOREIGN KEY (PROZ_TYPE_ID) REFERENCES RM_PROZ_TYPE (RM_PROZ_TYPE_ID);

-- changeset scolameo:1694179951382-31
ALTER TABLE RM_PROZ_COPY ADD CONSTRAINT RM_PROZ_ZIEL_RM_PROZ_DOK_FK FOREIGN KEY (PROZ_DOK_ADRESSE_ZIEL) REFERENCES RM_PROZ_DOK_INFO_COPY (RM_PROZ_DOK_INFO_ID);

-- changeset scolameo:1694179951382-32
CREATE OR REPLACE FORCE VIEW "RM_ADRESSE_ALL" ("VON_MUTDAT", "BIS_MUTDAT", "DELIDE", "RM_ADRESSE_ID", "RM_ADRESSE_TYP", "RM_ADRESSE_CODE", "VERSCHLUESSELUNG_TYP", "KOMPRIMIERUNG_TYP", "KONVERTIERUNG_TYP", "MUTIDE") AS select mutdat,
       date '9999-12-31',
       null,
       RM_ADRESSE_ID,
       RM_ADRESSE_TYP,
       RM_ADRESSE_CODE,
       VERSCHLUESSELUNG_TYP,
       KOMPRIMIERUNG_TYP,
       KONVERTIERUNG_TYP,
       MUTIDE
  from RM_ADRESSE
union all
select mutdat,
       ins_time,
       delide,
       RM_ADRESSE_ID,
       RM_ADRESSE_TYP,
       RM_ADRESSE_CODE,
       VERSCHLUESSELUNG_TYP,
       KOMPRIMIERUNG_TYP,
       KONVERTIERUNG_TYP,
       MUTIDE
  from RM_ADRESSE_HIST;

-- changeset scolameo:1694179951382-33
CREATE OR REPLACE FORCE VIEW "RM_ADRESSE_ARCHIV_OT_ALL" ("VON_MUTDAT", "BIS_MUTDAT", "DELIDE", "RM_ADRESSE_ID", "RM_ADRESSE_TYP", "LOGISCHES_ARCHIV", "ARCHIVSERVER", "ARCHIVSERVER_STANDBY", "MUTIDE") AS select mutdat,
       date '9999-12-31',
       null,
       RM_ADRESSE_ID,
       RM_ADRESSE_TYP,
       LOGISCHES_ARCHIV,
       ARCHIVSERVER,
       ARCHIVSERVER_STANDBY,
       MUTIDE
  from RM_ADRESSE_ARCHIV_OT
union all
select mutdat,
       ins_time,
       delide,
       RM_ADRESSE_ID,
       RM_ADRESSE_TYP,
       LOGISCHES_ARCHIV,
       ARCHIVSERVER,
       ARCHIVSERVER_STANDBY,
       MUTIDE
  from RM_ADRESSE_ARCHIV_OT_HIST;

-- changeset scolameo:1694179951382-34
CREATE OR REPLACE FORCE VIEW "RM_ADRESSE_DISK_WL_ALL" ("VON_MUTDAT", "BIS_MUTDAT", "DELIDE", "RM_ADRESSE_ID", "RM_ADRESSE_TYP", "DRIVE_MOUNT_PATH", "FOLDER_PATH", "MUTIDE") AS select mutdat,
       date '9999-12-31',
       null,
       RM_ADRESSE_ID,
       RM_ADRESSE_TYP,
       DRIVE_MOUNT_PATH,
       FOLDER_PATH,
       MUTIDE
  from RM_ADRESSE_DISK_WL
union all
select mutdat,
       ins_time,
       delide,
       RM_ADRESSE_ID,
       RM_ADRESSE_TYP,
       DRIVE_MOUNT_PATH,
       FOLDER_PATH,
       MUTIDE
  from RM_ADRESSE_DISK_WL_HIST;

-- changeset scolameo:1694179951382-35
CREATE OR REPLACE FORCE VIEW "RM_ADRESSE_NAS_ALL" ("VON_MUTDAT", "BIS_MUTDAT", "DELIDE", "RM_ADRESSE_ID", "RM_ADRESSE_TYP", "NETWORK_MOUNT_PATH", "FOLDER_PATH", "MUTIDE") AS select mutdat,
       date '9999-12-31',
       null,
       RM_ADRESSE_ID,
       RM_ADRESSE_TYP,
       NETWORK_MOUNT_PATH,
       FOLDER_PATH,
       MUTIDE
  from RM_ADRESSE_NAS
union all
select mutdat,
       ins_time,
       delide,
       RM_ADRESSE_ID,
       RM_ADRESSE_TYP,
       NETWORK_MOUNT_PATH,
       FOLDER_PATH,
       MUTIDE
  from RM_ADRESSE_NAS_HIST;

-- changeset scolameo:1694179951382-36
CREATE OR REPLACE FORCE VIEW "RM_ADRESSE_ORA_ALL" ("VON_MUTDAT", "BIS_MUTDAT", "DELIDE", "RM_ADRESSE_ID", "RM_ADRESSE_TYP", "DBMS_CONECTION_POOL_ALIAS", "SCHEMA_NAME", "TABLE_NAME", "TABLE_PK_SEQUENCE_NAME", "BLOB_COL_NAME", "MIMETYPE_COL_NAME", "PK_COL_NAME", "DBMS_HOST", "DBMS_PORT", "MUTIDE") AS select mutdat,
       date '9999-12-31',
       null,
       RM_ADRESSE_ID,
       RM_ADRESSE_TYP,
       DBMS_CONECTION_POOL_ALIAS,
       SCHEMA_NAME,
       TABLE_NAME,
       TABLE_PK_SEQUENCE_NAME,
       BLOB_COL_NAME,
       MIMETYPE_COL_NAME,
       PK_COL_NAME,
       DBMS_HOST,
       DBMS_PORT,
       MUTIDE
  from RM_ADRESSE_ORA
union all
select mutdat,
       ins_time,
       delide,
       RM_ADRESSE_ID,
       RM_ADRESSE_TYP,
       DBMS_CONECTION_POOL_ALIAS,
       SCHEMA_NAME,
       TABLE_NAME,
       TABLE_PK_SEQUENCE_NAME,
       BLOB_COL_NAME,
       MIMETYPE_COL_NAME,
       PK_COL_NAME,
       DBMS_HOST,
       DBMS_PORT,
       MUTIDE
  from RM_ADRESSE_ORA_HIST;

-- changeset scolameo:1694179951382-37
CREATE OR REPLACE FORCE VIEW "RM_PROZ_ALL" ("VON_MUTDAT", "BIS_MUTDAT", "DELIDE", "RM_PROZ_ID", "PROZ_TYPE_ID", "PROZ_DOK_ADRESSE_QUELLE", "PROZ_DOK_ADRESSE_ZIEL", "PROZ_STATUS_TS", "PROZ_STATUS_ID", "PROZ_FEHLERZAHL", "PROZ_FEHLER_TS", "PROZ_FEHLER_LOG", "DOK_HASH_CHECK_OK", "DOK_HASH_CHECK_OK_TS", "ZIEL_DOK_SEGMENT", "MUTIDE") AS select mutdat,
       date '9999-12-31',
       null,
       RM_PROZ_ID,
       PROZ_TYPE_ID,
       PROZ_DOK_ADRESSE_QUELLE,
       PROZ_DOK_ADRESSE_ZIEL,
       PROZ_STATUS_TS,
       PROZ_STATUS_ID,
       PROZ_FEHLERZAHL,
       PROZ_FEHLER_TS,
       PROZ_FEHLER_LOG,
       DOK_HASH_CHECK_OK,
       DOK_HASH_CHECK_OK_TS,
       ZIEL_DOK_SEGMENT,
       MUTIDE
  from RM_PROZ
union all
select mutdat,
       ins_time,
       delide,
       RM_PROZ_ID,
       PROZ_TYPE_ID,
       PROZ_DOK_ADRESSE_QUELLE,
       PROZ_DOK_ADRESSE_ZIEL,
       PROZ_STATUS_TS,
       PROZ_STATUS_ID,
       PROZ_FEHLERZAHL,
       PROZ_FEHLER_TS,
       PROZ_FEHLER_LOG,
       DOK_HASH_CHECK_OK,
       DOK_HASH_CHECK_OK_TS,
       ZIEL_DOK_SEGMENT,
       MUTIDE
  from RM_PROZ_HIST;

-- changeset scolameo:1694179951382-38
CREATE OR REPLACE FORCE VIEW "RM_PROZ_DOK_INFO_ALL" ("VON_MUTDAT", "BIS_MUTDAT", "DELIDE", "RM_PROZ_DOK_INFO_ID", "RM_ADRESSE_ID", "RM_ADRESSE_TYP", "DOK_ID", "MIMETYPE", "DATEINAME", "ANZ_BYTES", "DOK_HASH", "BEMERKUNG", "MUTIDE") AS select mutdat,
       date '9999-12-31',
       null,
       RM_PROZ_DOK_INFO_ID,
       RM_ADRESSE_ID,
       RM_ADRESSE_TYP,
       DOK_ID,
       MIMETYPE,
       DATEINAME,
       ANZ_BYTES,
       DOK_HASH,
       BEMERKUNG,
       MUTIDE
  from RM_PROZ_DOK_INFO
union all
select mutdat,
       ins_time,
       delide,
       RM_PROZ_DOK_INFO_ID,
       RM_ADRESSE_ID,
       RM_ADRESSE_TYP,
       DOK_ID,
       MIMETYPE,
       DATEINAME,
       ANZ_BYTES,
       DOK_HASH,
       BEMERKUNG,
       MUTIDE
  from RM_PROZ_DOK_INFO_HIST;

-- changeset scolameo:1694179951382-39
CREATE OR REPLACE FORCE VIEW "RM_PROZ_STATUS_ALL" ("VON_MUTDAT", "BIS_MUTDAT", "DELIDE", "RM_PROZ_STATUS_ID", "STATUS_NAME", "STATUS_CODE", "IST_IN_BEARBEITUNG", "IST_ERFOLGREICH_BEENDET", "HAT_QUELL_VERBINDUNG_FEHLER", "HAT_QUELL_ADRESSE_FEHLER", "HAT_QUELL_DOK_ID_FEHLER", "HAT_ZIEL_VERBINDUNG_FEHLER", "HAT_ZIEL_ADRESSE_FEHLER", "HAT_ZIEL_DOK_ID_FEHLER", "MUTIDE") AS select mutdat,
       date '9999-12-31',
       null,
       RM_PROZ_STATUS_ID,
       STATUS_NAME,
       STATUS_CODE,
       IST_IN_BEARBEITUNG,
       IST_ERFOLGREICH_BEENDET,
       HAT_QUELL_VERBINDUNG_FEHLER,
       HAT_QUELL_ADRESSE_FEHLER,
       HAT_QUELL_DOK_ID_FEHLER,
       HAT_ZIEL_VERBINDUNG_FEHLER,
       HAT_ZIEL_ADRESSE_FEHLER,
       HAT_ZIEL_DOK_ID_FEHLER,
       MUTIDE
  from RM_PROZ_STATUS
union all
select mutdat,
       ins_time,
       delide,
       RM_PROZ_STATUS_ID,
       STATUS_NAME,
       STATUS_CODE,
       IST_IN_BEARBEITUNG,
       IST_ERFOLGREICH_BEENDET,
       HAT_QUELL_VERBINDUNG_FEHLER,
       HAT_QUELL_ADRESSE_FEHLER,
       HAT_QUELL_DOK_ID_FEHLER,
       HAT_ZIEL_VERBINDUNG_FEHLER,
       HAT_ZIEL_ADRESSE_FEHLER,
       HAT_ZIEL_DOK_ID_FEHLER,
       MUTIDE
  from RM_PROZ_STATUS_HIST;

-- changeset scolameo:1694179951382-40
CREATE OR REPLACE FORCE VIEW "RM_PROZ_TYPE_ALL" ("VON_MUTDAT", "BIS_MUTDAT", "DELIDE", "RM_PROZ_TYPE_ID", "PROZESS_TYP_NAME", "PROZESS_TYP_CODE", "SYNC_MOVE_DOC", "SYNC_COPY_DOC", "SYNC_KEEP_TARGET_OLD_DOC", "SYNC_CHECK_SOURCE_TARGET_DOC", "SYNC_ENCHRYPT_TARGET_DOC", "SYNC_COMPRESS_TARGET_DOC", "SYNC_FORMAT_TARGET_DOC", "VIRUSSCAN_TARGET_DOC", "RETENTION_DELETE_TARGET_DOC", "MUTIDE") AS select mutdat,
       date '9999-12-31',
       null,
       RM_PROZ_TYPE_ID,
       PROZESS_TYP_NAME,
       PROZESS_TYP_CODE,
       SYNC_MOVE_DOC,
       SYNC_COPY_DOC,
       SYNC_KEEP_TARGET_OLD_DOC,
       SYNC_CHECK_SOURCE_TARGET_DOC,
       SYNC_ENCHRYPT_TARGET_DOC,
       SYNC_COMPRESS_TARGET_DOC,
       SYNC_FORMAT_TARGET_DOC,
       VIRUSSCAN_TARGET_DOC,
       RETENTION_DELETE_TARGET_DOC,
       MUTIDE
  from RM_PROZ_TYPE
union all
select mutdat,
       ins_time,
       delide,
       RM_PROZ_TYPE_ID,
       PROZESS_TYP_NAME,
       PROZESS_TYP_CODE,
       SYNC_MOVE_DOC,
       SYNC_COPY_DOC,
       SYNC_KEEP_TARGET_OLD_DOC,
       SYNC_CHECK_SOURCE_TARGET_DOC,
       SYNC_ENCHRYPT_TARGET_DOC,
       SYNC_COMPRESS_TARGET_DOC,
       SYNC_FORMAT_TARGET_DOC,
       VIRUSSCAN_TARGET_DOC,
       RETENTION_DELETE_TARGET_DOC,
       MUTIDE
  from RM_PROZ_TYPE_HIST;

